package tcplugins;

import static org.junit.Assert.*;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;

import org.junit.Test;

public class LGPFileTestcase {

	@Test
	public void testFilecheckTrue() throws Exception {
		URL testFile = getClass().getClassLoader().getResource("magic.lgp");
		boolean result = LGPFile.isLGPFile(testFile.getPath());
		assertTrue(result);
	}

	@Test
	public void testFilecheckFalse() throws Exception {
		URL testFile = getClass().getClassLoader().getResource("tcplugins/LGPFileTestcase.class");
		boolean result = LGPFile.isLGPFile(testFile.getPath());
		assertFalse(result);
	}

	@Test
	public void testOpenClose() throws Exception {
		URL testFile = getClass().getClassLoader().getResource("magic.lgp");
		LGPFile l = new LGPFile(testFile.getPath());
		l.open();
		assertEquals(5252, l.getNumberEntries());
		l.close();
	}

	@Test
	public void testListingWithoutPathnames() throws Exception {
		URL testFile = getClass().getClassLoader().getResource("moviecam.lgp");
		LGPFile l = new LGPFile(testFile.getPath());
		l.open();

		int i = 0;
		LGPFileEntry e = (LGPFileEntry) l.readNextEntry();
		while (e != null) {
			assertNotNull(e.getFilename());
			assertNotEquals(e.getOffset(), 0);
			assertNotEquals(e.getSize(), 0);
			System.out.println(e.getCompleteFilename() + " " + e.getSize() + " " + e.getOffset());
			i++;
			e = (LGPFileEntry) l.readNextEntry();
		}
		assertEquals(124, i);
		l.close();
	}

	@Test
	public void testListingWithPathnames() throws Exception {
		URL testFile = getClass().getClassLoader().getResource("magic.lgp");
		LGPFile l = new LGPFile(testFile.getPath());
		l.open();

		int i = 0;
		LGPFileEntry e = (LGPFileEntry) l.readNextEntry();
		while (e != null) {
			assertNotNull(e.getFilename());
			assertNotEquals(e.getOffset(), 0);
			assertNotEquals(e.getSize(), 0);
			System.out.println(e.getCompleteFilename() + " " + e.getSize() + " " + e.getOffset());
			i++;
			e = (LGPFileEntry) l.readNextEntry();
		}
		assertEquals(5252, i);
		l.close();
	}

	@Test
	public void testExtraction() throws Exception {
		URL testFile = getClass().getClassLoader().getResource("moviecam.lgp");
		LGPFile l = new LGPFile(testFile.getPath());
		l.open();

		int i = 0;
		LGPFileEntry e = (LGPFileEntry) l.readNextEntry();
		while (e != null) {
			assertNotNull(e.getFilename());
			assertNotEquals(e.getOffset(), 0);
			assertNotEquals(e.getSize(), 0);
			File temp = File.createTempFile(e.getFilename(), ".test");
			temp.deleteOnExit();
			l.extractCurrentEntry(temp);
			assertEquals(e.getSize(), temp.length());
			i++;
			e = (LGPFileEntry) l.readNextEntry();
		}
		assertEquals(124, i);
		l.close();
	}

	@Test
	public void testAddFilesWithoutPathname() throws Exception {
		URL testFile = getClass().getClassLoader().getResource("moviecam.lgp");
		File orig = new File(testFile.getPath());
		File dest = new File(orig.getParentFile(), "moviecam2.lgp");
		Files.copy(orig.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
		LGPWriter writer = new LGPWriter(dest.getAbsolutePath());
		writer.addFilesToArchive("src/tcplugins", null, Arrays.asList(new String[] { "LGPFileEntry.java", "LGPFile.java" }));
	}

	@Test
	public void testAddFilesWithPathname() throws Exception {
		URL testFile = getClass().getClassLoader().getResource("moviecam.lgp");
		File orig = new File(testFile.getPath());
		File dest = new File(orig.getParentFile(), "moviecam3.lgp");
		Files.copy(orig.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
		LGPWriter writer = new LGPWriter(dest.getAbsolutePath());
		writer.addFilesToArchive("src/tcplugins", "test", Arrays.asList(new String[] { "LGPFileEntry.java", "LGPFile.java" }));
	}

	@Test
	public void testReplaceFilesWithoutPathname() throws Exception {
		URL testFile = getClass().getClassLoader().getResource("moviecam.lgp");
		File orig = new File(testFile.getPath());
		File dest = new File(orig.getParentFile(), "moviecam4.lgp");
		Files.copy(orig.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
		LGPWriter writer = new LGPWriter(dest.getAbsolutePath());
		writer.addFilesToArchive("src/tcplugins", null, Arrays.asList(new String[] { "LGPFileEntry.java", "LGPFile.java" }));
		writer.addFilesToArchive("src/tcplugins", null, Arrays.asList(new String[] { "LGPFileEntry.java", "LGPFile.java" }));
	}

	@Test
	public void testReplaceFilesWithPathname() throws Exception {
		URL testFile = getClass().getClassLoader().getResource("moviecam.lgp");
		File orig = new File(testFile.getPath());
		File dest = new File(orig.getParentFile(), "moviecam5.lgp");
		Files.copy(orig.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
		LGPWriter writer = new LGPWriter(dest.getAbsolutePath());
		writer.addFilesToArchive("src/tcplugins", "test", Arrays.asList(new String[] { "LGPFileEntry.java", "LGPFile.java" }));
		writer.addFilesToArchive("src/tcplugins", "test", Arrays.asList(new String[] { "LGPFileEntry.java", "LGPFile.java" }));
	}

	@Test
	public void testDeleteFilesWithoutPathname() throws Exception {
		URL testFile = getClass().getClassLoader().getResource("moviecam4.lgp");
		File orig = new File(testFile.getPath());
		File dest = new File(orig.getParentFile(), "moviecam6.lgp");
		Files.copy(orig.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
		LGPWriter writer = new LGPWriter(dest.getAbsolutePath());
		writer.deleteFilesFromArchive(Arrays.asList(new String[] { "LGPFileEntry.java", "LGPFile.java" }));
	}

	@Test
	public void testDeleteFilesWithPathname() throws Exception {
		URL testFile = getClass().getClassLoader().getResource("moviecam5.lgp");
		File orig = new File(testFile.getPath());
		File dest = new File(orig.getParentFile(), "moviecam7.lgp");
		Files.copy(orig.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
		LGPWriter writer = new LGPWriter(dest.getAbsolutePath());
		writer.deleteFilesFromArchive(Arrays.asList(new String[] { "test\\LGPFileEntry.java", "test\\LGPFile.java" }));
	}
}
