package tcplugins;

import java.io.File;

public abstract class FileEntry {
	private String pathname;
	private String filename;
	private byte type;
	private int size;

	public FileEntry(String filename, byte type) {
		this.filename = filename;
		this.type = type;
	}

	public byte getType() {
		return type;
	}

	public String getFilename() {
		return filename;
	}

	public String getCompleteFilename() {
		if (pathname != null) {
			return pathname + File.separator + filename;
		}
		return filename;
	}

	public String getPathname() {
		return pathname;
	}

	public int getSize() {
		return size;
	}

	public void setPathname(String pathname) {
		this.pathname = pathname;
	}

	public void setSize(int size) {
		this.size = size;
	}

}
