package tcplugins;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import plugins.wcx.HeaderData;
import plugins.wcx.OpenArchiveData;
import plugins.wcx.WCXPluginAdapter;

public class LGPWCXPlugin extends WCXPluginAdapter {
	private static final Log log = LogFactory.getLog(LGPWCXPlugin.class);

	@Override
	public int getPackerCaps() {
		if (log.isDebugEnabled()) {
			log.debug("calling getPackerCaps()");
		}
		return PK_CAPS_NEW | PK_CAPS_MODIFY | PK_CAPS_DELETE | PK_CAPS_MULTIPLE | PK_CAPS_BY_CONTENT;
	}

	@Override
	public Object openArchive(OpenArchiveData archiveData) {
		if (log.isDebugEnabled()) {
			log.debug("calling openArchive(" + archiveData.getArcName() + ")");
		}
		try {
			LGPFile f = new LGPFile(archiveData.getArcName());
			f.open();
			return f;
		} catch (Exception e) {
			log.error(archiveData.getArcName(), e);
			return null;
		}
	}

	@Override
	public int readHeader(Object archiveData, HeaderData headerData) {
		LGPFile archive = (LGPFile) archiveData;

		if (log.isDebugEnabled()) {
			log.debug("calling readHeader(" + archive.getFilename() + ")");
		}

		try {
			FileEntry entry = archive.readNextEntry();
			if (entry == null) {
				return E_END_ARCHIVE;
			}
			headerData.setArcName(archive.getFilename());
			headerData.setFileName(entry.getCompleteFilename());
			headerData.setPackSize(entry.getSize());
			headerData.setUnpSize(entry.getSize());
		} catch (Exception e) {
			log.error(archive.getFilename(), e);
			return E_EREAD;
		}
		return SUCCESS;
	}

	@Override
	public int processFile(Object archiveData, int operation, String destPath, String destName) {
		LGPFile archive = (LGPFile) archiveData;

		if (log.isDebugEnabled()) {
			log.debug("calling processFile(" + archive.getFilename() + ", " + operation + ", " + destPath + ", "
				+ destName + ")");
		}

		switch (operation) {
			case PK_SKIP:
				break;
			case PK_TEST:
				break;
			case PK_EXTRACT:
				File destination;
				if (destPath == null) {
					destination = new File(destName);
				} else {
					destination = new File(destPath, destName);
				}
				try {
					archive.extractCurrentEntry(destination);
				} catch (Exception e) {
					log.error(archive.getFilename(), e);
					return E_EWRITE;
				}
				break;
			default:
				return E_NOT_SUPPORTED;
		}
		return SUCCESS;
	}

	@Override
	public int closeArchive(Object archiveData) {
		LGPFile archive = (LGPFile) archiveData;

		if (log.isDebugEnabled()) {
			log.debug("calling closeArchive(" + archive.getFilename() + ")");
		}

		try {
			archive.close();
		} catch (Exception e) {
			log.error(archive.getFilename(), e);
			return E_ECLOSE;
		}
		return SUCCESS;
	}

	@Override
	public int packFiles(String packedFile, String subPath, String srcPath, String addList, int flags) {
		if (log.isDebugEnabled()) {
			log.debug("calling packFiles(" + packedFile + ", " + subPath + ", " + srcPath + ", " + addList + ", "
				+ flags + ")");
		}

		List<String> filenames = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(addList, ":");
		while (st.hasMoreTokens()) {
			String filename = st.nextToken();
			File f = new File(srcPath, filename);
			if (!f.isFile()) {
				continue;
			}
			if (f.getName().length() > 20) {
				log.warn("name of " + filename + " is longer than 20 chars, ignoring");
				continue;
			}
			if (f.length() > Integer.MAX_VALUE) {
				log.warn(filename + " is larger than 2G, ignoring");
				continue;
			}
			filenames.add(filename);
		}

		LGPWriter writer = new LGPWriter(packedFile);
		try {
			writer.addFilesToArchive(srcPath, subPath, filenames);
		} catch (Exception e) {
			log.error(packedFile, e);
			return E_ECREATE;
		}
		return SUCCESS;
	}

	@Override
	public int deleteFiles(String packedFile, String deleteList) {
		if (log.isDebugEnabled()) {
			log.debug("calling deleteFiles(" + packedFile + ", " + deleteList + ")");
		}

		List<String> filenames = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(deleteList, ":");
		while (st.hasMoreTokens()) {
			filenames.add(st.nextToken());
		}

		LGPWriter writer = new LGPWriter(packedFile);
		try {
			writer.deleteFilesFromArchive(filenames);
		} catch (Exception e) {
			log.error(packedFile, e);
			return E_ECREATE;
		}
		return SUCCESS;
	}

	@Override
	public boolean canYouHandleThisFile(String fileName) {
		if (log.isDebugEnabled()) {
			log.debug("calling canYouHandleThisFile(" + fileName + ")");
		}

		try {
			return LGPFile.isLGPFile(fileName);
		} catch (Exception e) {
			log.error(fileName, e);
			return false;
		}
	}
}
