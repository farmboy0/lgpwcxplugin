package tcplugins;

public class LGPFileEntry extends FileEntry {
	private int offset;
	private short pathindex;

	public LGPFileEntry(String filename, byte type, int offset, short pathIndex) {
		super(filename, type);
		this.offset = offset;
		this.pathindex = pathIndex;
	}

	public int getOffset() {
		return offset;
	}

	public short getPathindex() {
		return pathindex;
	}

}
