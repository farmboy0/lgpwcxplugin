package tcplugins;

import java.io.File;

public class ExtFileEntry extends FileEntry {
	private File extFile;

	public ExtFileEntry(File extFile) {
		super(extFile.getName(), (byte) 0xE);
		this.extFile = extFile;
	}

	public File getExtFile() {
		return extFile;
	}

}
