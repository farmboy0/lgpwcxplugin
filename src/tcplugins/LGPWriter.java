package tcplugins;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class LGPWriter {
	private String filename;

	public LGPWriter(String filename) {
		this.filename = filename;
	}

	public void addFilesToArchive(String srcPath, String dstPath, List<String> filenames) throws IOException {
		File newArchive = null;
		try (LGPFile archive = new LGPFile(filename)) {
			archive.open();

			Set<String> filesWithPathnames = new HashSet<String>();
			Map<String, FileEntry> entries = new TreeMap<String, FileEntry>();

			FileEntry entry = archive.readNextEntry();
			while (entry != null) {
				entries.put(entry.getCompleteFilename(), entry);
				if (entry.getPathname() != null) {
					filesWithPathnames.add(entry.getCompleteFilename());
				}
				entry = archive.readNextEntry();
			}

			for (String filename : filenames) {
				File f = new File(srcPath, filename);
				entry = new ExtFileEntry(f);

				String fnPath = filename.lastIndexOf('\\') != -1 ? filename.substring(0, filename.lastIndexOf('\\')) : null;
				if (dstPath != null || fnPath != null) {
					entry.setPathname((dstPath != null ? dstPath : "")
						+ (dstPath != null && fnPath != null ? "\\" : "") + (fnPath != null ? fnPath : ""));
					filesWithPathnames.add(entry.getCompleteFilename());
				}
				entry.setSize((int) f.length());

				entries.put(entry.getCompleteFilename(), entry);
			}
			newArchive = writeNewArchive(archive, entries, filesWithPathnames);
		}
		replaceArchive(newArchive);
	}

	public void deleteFilesFromArchive(List<String> filenames) throws IOException {
		File newArchive = null;
		try (LGPFile archive = new LGPFile(filename)) {
			archive.open();

			Set<String> filesWithPathnames = new HashSet<String>();
			Map<String, FileEntry> entries = new TreeMap<String, FileEntry>();

			FileEntry entry = archive.readNextEntry();
			while (entry != null) {
				if (!filenames.contains(entry.getCompleteFilename())) {
					entries.put(entry.getCompleteFilename(), entry);
					if (entry.getPathname() != null) {
						filesWithPathnames.add(entry.getCompleteFilename());
					}
				}
				entry = archive.readNextEntry();
			}
			newArchive = writeNewArchive(archive, entries, filesWithPathnames);
		}
		replaceArchive(newArchive);
	}

	public File writeNewArchive(LGPFile archive, Map<String, FileEntry> entries, Set<String> filesWithPathnames)
		throws IOException {

		ByteBuffer header = ByteBuffer.wrap(LGPFile.LGP_START_MARKER.getBytes());
		ByteBuffer footer = ByteBuffer.wrap(LGPFile.LGP_END_MARKER.getBytes());
		ByteBuffer hashes = ByteBuffer.wrap(archive.readHashes());

		ByteBuffer toc = ByteBuffer.allocate(4 + LGPFile.LGP_HEADER_ENTRY_SIZE * entries.size());
		writeInt(toc, entries.size());

		ByteBuffer pathes = ByteBuffer.allocate(2 + 2 + LGPFile.LGP_PATHES_ENTRY_SIZE * filesWithPathnames.size());
		writeShort(pathes, (short) (filesWithPathnames.isEmpty() ? 0 : 1));
		writeShort(pathes, (short) filesWithPathnames.size());

		int offset = LGPFile.LGP_START_MARKER.length() + 4 + LGPFile.LGP_HEADER_ENTRY_SIZE * entries.size()
			+ LGPFile.LGP_HASHES_SIZE + 2 + 2 + LGPFile.LGP_PATHES_ENTRY_SIZE * filesWithPathnames.size();

		short tocIndex = 0, pathesIndex = 0;
		for (String name : entries.keySet()) {
			FileEntry entry = entries.get(name);

			toc.put(entry.getFilename().getBytes());
			toc.position(4 + tocIndex * LGPFile.LGP_HEADER_ENTRY_SIZE + 20);

			writeInt(toc, offset);
			offset += 24;
			offset += entry.getSize();

			toc.put(entry.getType());
			if (entry.getPathname() != null) {
				writeShort(toc, (short) 1);
				pathes.put(entry.getPathname().replace('\\', '/').getBytes());
				pathes.position(2 + 2 + pathesIndex * LGPFile.LGP_PATHES_ENTRY_SIZE + 128);
				writeShort(pathes, tocIndex);
				pathesIndex++;
			} else {
				writeShort(toc, (short) 0);
			}

			tocIndex++;
		}
		toc.flip();
		pathes.flip();

		File newArchive = File.createTempFile("lgp", ".temp");
		try (FileOutputStream fos = new FileOutputStream(newArchive)) {
			FileChannel fosChannel = fos.getChannel();
			fosChannel.write(header);
			fosChannel.write(toc);
			fosChannel.write(hashes);
			fosChannel.write(pathes);
			for (String name : entries.keySet()) {
				FileEntry entry = entries.get(name);
				ByteBuffer nameSize = ByteBuffer.allocate(24);
				nameSize.put(entry.getFilename().getBytes());
				nameSize.position(20);
				writeInt(nameSize, entry.getSize());
				nameSize.flip();
				fosChannel.write(nameSize);
				if (entry instanceof LGPFileEntry) {
					archive.extractEntry((LGPFileEntry) entry, fosChannel);
				} else {
					FileInputStream fis = new FileInputStream(((ExtFileEntry) entry).getExtFile());
					fosChannel.transferFrom(fis.getChannel(), fosChannel.position(), entry.getSize());
					fis.close();
				}
				fosChannel.position(fosChannel.position() + entry.getSize());
			}
			fosChannel.write(footer);
		}
		return newArchive;
	}

	private void replaceArchive(File replacement) {
		File archiveFile = new File(filename);
		archiveFile.delete();
		replacement.renameTo(archiveFile);
	}

	private void writeShort(ByteBuffer b, short s) {
		b.put((byte) (s & 0x00FF));
		b.put((byte) ((s & 0xFF00) >> 8));
	}

	private void writeInt(ByteBuffer b, int i) {
		b.put((byte) (i & 0x000000FF));
		b.put((byte) ((i & 0x0000FF00) >> 8));
		b.put((byte) ((i & 0x00FF0000) >> 16));
		b.put((byte) ((i & 0xFF000000) >> 24));
	}
}
