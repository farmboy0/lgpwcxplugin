package tcplugins;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;

public class LGPFile implements Closeable {
	public static final String LGP_START_MARKER = "\0\0SQUARESOFT";
	public static final String LGP_END_MARKER = "FINAL FANTASY7";

	public static final int LGP_HEADER_ENTRY_SIZE = 27;
	public static final int LGP_PATHES_ENTRY_SIZE = 130;
	public static final int LGP_HASHES_SIZE = 3600;

	private String filename;
	private RandomAccessFile archiveFile = null;
	private Map<Integer, String> paths = new HashMap<Integer, String>();
	private int numberEntries = 0;

	private int currentEntryIndex = -1;
	private LGPFileEntry currentEntry = null;

	public static boolean isLGPFile(String filename) throws IOException {
		testFilenameUsability(filename);
		RandomAccessFile f = new RandomAccessFile(filename, "r");
		f.seek(f.length() - LGP_END_MARKER.length());
		byte[] marker = new byte[LGP_END_MARKER.length()];
		f.read(marker);
		f.close();
		return LGP_END_MARKER.equals(new String(marker));
	}

	public static void testFilenameUsability(String filename) {
		if (filename == null || filename.trim().isEmpty()) {
			throw new IllegalArgumentException("no filename given");
		}
		File archive = new File(filename);
		if (!archive.exists() || !archive.canRead() || !archive.isFile()) {
			throw new IllegalArgumentException("file cant be read or isnt a file");
		}
	}

	public LGPFile(String filename) {
		testFilenameUsability(filename);
		this.filename = filename;
	}

	public String getFilename() {
		return filename;
	}

	public int getCurrentEntryIndex() {
		return currentEntryIndex;
	}

	public int getNumberEntries() {
		return numberEntries;
	}

	public void open() throws IOException {
		archiveFile = new RandomAccessFile(getFilename(), "r");
		readNumberOfTOCEntries();
		readPathnames();
	}

	public void close() throws IOException {
		currentEntryIndex = -1;
		currentEntry = null;
		archiveFile.close();
	}

	public FileEntry readNextEntry() throws IOException {
		currentEntryIndex++;

		if (currentEntryIndex == numberEntries) {
			return null;
		}

		archiveFile.seek(12 + 4 + 27 * currentEntryIndex);

		currentEntry = readTOCEntry();
		updateEntrySize(currentEntry);
		currentEntry.setPathname(paths.get(currentEntryIndex));

		return currentEntry;
	}

	public void extractCurrentEntry(File destination) throws IOException {
		FileOutputStream fos = new FileOutputStream(destination);
		extractEntry(currentEntry, fos.getChannel());
		fos.close();
	}

	public void extractEntry(LGPFileEntry entry, FileChannel output) throws IOException {
		archiveFile.seek(entry.getOffset() + 24);

		output.transferFrom(archiveFile.getChannel(), output.position(), entry.getSize());
	}

	public byte[] readHashes() throws IOException {
		archiveFile.seek(12 + 4 + (numberEntries * 27));

		byte[] hashes = new byte[3600];
		archiveFile.read(hashes);

		return hashes;
	}

	private void readNumberOfTOCEntries() throws IOException {
		archiveFile.seek(12);
		numberEntries |= archiveFile.readUnsignedByte();
		numberEntries |= (archiveFile.readUnsignedByte() << 8);
		numberEntries |= (archiveFile.readUnsignedByte() << 16);
		numberEntries |= (archiveFile.readUnsignedByte() << 24);
	}

	private void readPathnames() throws IOException {
		archiveFile.seek(12 + 4 + (numberEntries * 27) + 3600);

		int numberOfPaths = 0;
		numberOfPaths |= archiveFile.readUnsignedByte();
		numberOfPaths |= (archiveFile.readUnsignedByte() << 8);

		for (int i = 0; i < numberOfPaths; i++) {
			int numberOfPathEntries = 0;
			numberOfPathEntries |= archiveFile.readUnsignedByte();
			numberOfPathEntries |= (archiveFile.readUnsignedByte() << 8);
			for (int j = 0; j < numberOfPathEntries; j++) {
				byte[] pathname = new byte[128];
				archiveFile.read(pathname);

				String strPathname = byteArray2String(pathname).replace('/', File.separatorChar);

				int entryIndex = 0;
				entryIndex |= archiveFile.readUnsignedByte();
				entryIndex |= (archiveFile.readUnsignedByte() << 8);

				paths.put(entryIndex, strPathname);
			}
		}
	}

	private LGPFileEntry readTOCEntry() throws IOException {
		byte[] name = new byte[20];
		archiveFile.read(name);

		int offset = 0;
		offset |= archiveFile.readUnsignedByte();
		offset |= (archiveFile.readUnsignedByte() << 8);
		offset |= (archiveFile.readUnsignedByte() << 16);
		offset |= (archiveFile.readUnsignedByte() << 24);

		byte type = archiveFile.readByte();

		short pathindex = 0;
		pathindex |= archiveFile.readUnsignedByte();
		pathindex |= (archiveFile.readUnsignedByte() << 8);

		return new LGPFileEntry(byteArray2String(name), type, offset, pathindex);
	}

	private void updateEntrySize(LGPFileEntry entry) throws IOException {
		archiveFile.seek(entry.getOffset());

		byte[] name = new byte[20];
		archiveFile.read(name);

		int size = 0;
		size |= archiveFile.readUnsignedByte();
		size |= (archiveFile.readUnsignedByte() << 8);
		size |= (archiveFile.readUnsignedByte() << 16);
		size |= (archiveFile.readUnsignedByte() << 24);

		entry.setSize(size);
	}

	private String byteArray2String(byte[] bytes) {
		int length = 0;
		while (bytes[length] != 0) {
			length++;
		}
		return new String(bytes, 0, length);
	}
}
