Total Commander archiver plugin for Final Fantasy 7 LGP archives
----------------------------------------------------------------

Supported Operations
--------------------
1.List files
2.Extract files
3.Check for supported files
4.Add/Overwrite files
5.Delete files(buggy, will only delete one file at a time)

How-To Install
--------------
1.Install Java JRE from http://www.oracle.com/technetwork/java/javase/downloads/index.html
2.Unpack the javalib.tgz into Total Commander's install directory
3.Edit the javalib/tc_javaplugin.ini, uncomment both entries and set to the correct values
4.Open the release zip with Total Commander, it should ask you if you want to install the plugin
5.Accept the standard values
